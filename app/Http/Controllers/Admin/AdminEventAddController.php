<?php

namespace App\Http\Controllers\Admin;

use App\Event_category;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Event;
use App\User;
use App\Departament;
use App\Http\Controllers\Controller;


class AdminEventAddController extends Controller
{
    public function execute(Request $request)
    {
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $user_departament_id = User::find($user->id)->departament->id;
        $dep_name_user = User::find($user->id)->departament->name;

        if ($user_role_id == User::ROLE_ADMIN) {

            if ($request->isMethod('post')) {

                $input = $request->except('_token');

                $messages = [
                    'required' => 'Поле :attribute обязательно к заполнению',
                    'unique' => 'Поле :attribute должно быть уникальным'
                ];

                $validator = Validator::make($input, [

                    'title' => 'required|max:255|unique:events',
                    'start' => 'required'
                ], $messages);

                if ($validator->fails()) {
                    return redirect()->route('eventAdd')->withErrors($validator)->withInput();
                }

                $user = Auth::user();

                $event = new Event();

                $event->fill($input);

                $event->creator_id = $user->id;

                if ($event->save()) {

                    if ($request->ajax()) {

                        $message = "Saved Success!";

                        return $message;

                    } else {
                        return redirect('admin')->with('status', 'Event was create');
                    }
                }
            }

            $category = Event_category::all();
            $users = User::all();
            $departaments = Departament::all();
            $perfomers = User::all();

            if (view()->exists('admin.events.eventAddIndex')) {
                $data = [
                    'title' => 'New Event',
                    'category' => $category,
                    'users' => $users,
                    'departaments' => $departaments,
                    'perfomers' => $perfomers
                ];
                return view('admin.events.eventAddIndex', $data);
            }
            abort(404);

        } elseif ($user_role_id == 2) {

            if ($request->isMethod('post')) {

                $input = $request->except('_token');

                $messages = [
                    'required' => 'Поле :attribute обязательно к заполнению',
                    'unique' => 'Поле :attribute должно быть уникальным'
                ];

                $validator = Validator::make($input, [
                    'title' => 'required|max:255',
                ], $messages);

                if ($validator->fails()) {
                    return redirect()->route('eventAdd')->withErrors($validator)->withInput();
                }

                $user = Auth::user();

                $event = new Event();

                $event->fill($input);

                $event->creator_id = $user->id;

                if ($event->save()) {

                    if ($request->ajax()) {

                        $message = "Success!";

                        return $message;

                    } else {
                        return redirect('admin')->with('status', 'Event was create');
                    }
                }
            }

            $category = Event_category::all();
            $users = User::where('departament_id', $user_departament_id)->get();
            $departaments = Departament::where('id', $user_departament_id)->get();

            if (view()->exists('admin.events.eventAddIndex')) {
                $data = [
                    'title' => 'New Event',
                    'category' => $category,
                    'users' => $users,
                    'departaments' => $departaments
                ];
                return view('admin.events.eventAddIndex', $data);
            }
            abort(404);

        } else {

            if ($request->isMethod('post')) {

                $input = $request->except('_token');

                $messages = [
                    'required' => 'Поле :attribute обязательно к заполнению',
                    'unique' => 'Поле :attribute должно быть уникальным'
                ];

                $validator = Validator::make($input, [

                    'title' => 'required|max:255|unique:events',
                    'start' => 'required'
                ], $messages);

                if ($validator->fails()) {
                    return redirect()->route('eventAdd')->withErrors($validator)->withInput();
                }

                $event = new Event();

                $event->fill($input);

                $event->save();
            }

        }
    }
}
