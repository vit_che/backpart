<?php

namespace App\Http\Controllers\Admin;

use App\Departament;
use Illuminate\Http\Request;
use App\Event;
use App\User;
use Auth;
use Validator;
use App\Http\Controllers\Controller;

class AdminEventEditController extends Controller
{
    public function execute(Event $event, Request $request)
    {
        //ONLY FOR ADMIN!!
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;

        if ($user_role_id == User::ROLE_ADMIN) {

            if ($request->isMethod('DELETE')) {
                $event->delete();
                return redirect('admin')->with('status', 'Event was deleted');
            }
        } else {
            //сообщение о превышении Прав
            $mistake = 'You don`t have a right to DELETE Event!';
            return redirect('admin')->withErrors($mistake);
        }

        if($request->isMethod('POST')){
            $input = $request->except('_token');
            $validator = Validator::make($input, [
                'title' => 'required|max:255',
                'description' => 'required',
            ]);
            if($validator->fails()){
                return redirect()
                    ->route('eventEdit', ['event'=>$input['id']])
                    ->withErrors($validator);
            }

            $event->fill($input);

            if($event->update()) {
                return redirect('admin')->with('status', 'Event was update');
            }
        }

        $old = $event->toArray();
        $performers = User::all();
        $departaments = Departament::all();

        $old_creator_name = Event::find($event->id)->creator->name;

        $old_departament_name = Event::find($event->id)->departament->name;
        $old_departament_id = Event::find($event->id)->departament->id;

        $old_event_perfomer_name = Event::find($event->id)->user->name;
        $old_event_perfomer_id = Event::find($event->id)->user->id;

        dump($old_creator_name);

        if(view()->exists('admin.events.eventEditIndex')){
            $data = [
                'title' => 'Editing event "'.$old['title'].'"',
                'data' => $old,
                'performers' => $performers,
                'departaments' => $departaments,
                'old_departament_name' => $old_departament_name,
                'old_departament_id' => $old_departament_id,
                'old_event_perfomer_name' => $old_event_perfomer_name,
                'old_event_perfomer_id' => $old_event_perfomer_id,
                'old_creator_name' => $old_creator_name
            ];
            return view('admin.events.eventEditIndex', $data);
        }
    }
}
