<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Departament;
use App\User;
use Validator;
use Auth;
use App\Event;



class DepartamentController extends Controller
{
    public function depsdata()
    {
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $user_dep_id = User::find($user->id)->departament->id;

        if ($user_role_id == User::ROLE_ADMIN) {

            $departaments = Departament::all();
            $users = User::all();

            $data = [
                'users' => $users,
                'deps' => $departaments
            ];

            return $data;

        } elseif ($user_role_id == User::ROLE_MODER) {

            $departaments = [];
            $departaments[] = Departament::find($user_dep_id);
            $users = User::where('departament_id', $user_dep_id)->get();

            $arr_users = [];

            if(count($users) == 1 ){

                $arr_users[] = $users;

                $data = [
                    'deps' => $departaments,
                    'users' => $arr_users
                ];

                return $data;

            }

            $data = [
                'deps' => $departaments,
                'users' => $users
            ];

            return $data;

        } else {

            $message = "У Вас нет Прав";

            return $message;
        }
    }

    public function depadd(Request $request){

        if ($request->isMethod('post')) {

            $input = $request->except('_token');

            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'unique' => 'Поле :attribute должно быть уникальным',
                'max' => 'Поле :attribute должно быть не более 100 символов'
            ];

            $validator = Validator::make($input,[
                'name' => 'required|max:100|unique:departaments',
            ], $messages);

            if($validator->fails()) {

                return $this->validatorFails($validator);
            }

            $departament = new Departament();

            $departament->fill($input);

            if($departament->save()) {

                $successmsg = "Отдел создан";

                return $successmsg;
            }
        }
    }


    public function getdep (Request $request){

        $id = $request->id;
        $dep = Departament::find($id);

        return $dep;
    }


    public function edit(Request $request){

        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $dep_id = $request->id;
        $dep_edit = Departament::find($dep_id);

        if ($user_role_id != User::ROLE_ADMIN) {
            //сообщение о превышении Прав
            $mistake = 'You don`t have a right to Edit User!';

            return $mistake;

        } elseif ( $user_role_id == User::ROLE_ADMIN ){

            if ($request->isMethod('DELETE')) {

                    if(!isset($request->id)) {

                        $message = "Выберите Отдел!";

                        return $message;
                    }

                $dep_events = Departament::find($dep_id)->events;
                $dep_users = Departament::find($dep_id)->users;

                $events_title = []; // related events
                foreach ($dep_events as $event_title) {
                    $events_title[] = $event_title->title;
                }

                $users_name = []; // related events
                foreach ($dep_users as $user_name) {
                    $users_name[] = $user_name->name;
                }

                if ((count($events_title) > 0) || (count($users_name) > 0)) {

                    $dep_has_depens = "Отдел имеет зависимые События и/или Пользователей!
                         Удалить Отдел вместе с зависимыми Событиями и пользователями?";

                    $data = [
                        "depens" => true,
                        "msg" => $dep_has_depens,
                        "events_title" => $events_title,
                        "users_name" => $users_name
                    ];

                    return $data;
                }

                if($dep_edit->delete()) {

                    $msg = 'Отдел Удален';

                    return $msg;

                } else {

                    $msg = 'Отдел НЕ был Удален!!!';

                    return $msg;
                }

            } elseif ($request->isMethod('POST')){

                $input = $request->except('_token');

                $messages = [
                    'required' => 'Поле :attribute обязательно к заполнению',
                    'max' => "длина превышает 25 символов"
                ];

                $validator = Validator::make($input, [

                    'name' => 'required|max:25',
                    'color' => 'required',

                ], $messages);

                if($validator->fails()){

                    return $this->validatorFails($validator);
                }

                $dep_edit->fill($input);

                if($dep_edit->update()) {

                    $msg = 'Изменения Сохранены';

                    return $msg;
                }
            }
        }
    }

    public function delDepDepenc(Request $request)
    {
        $user = Auth::user();
        $user_role_id = User::find($user->id)->role->id;
        $dep_id = $request->id;
        $dep_edit = Departament::find($dep_id);
        $dep_events = Departament::find($dep_id)->events;
        $dep_users = Departament::find($dep_id)->users;

        if ($user_role_id != User::ROLE_ADMIN) {
            //сообщение о превышении Прав
            $message = 'You don`t have a right to Edit User!';

            return $message;

        } elseif ($user_role_id == User::ROLE_ADMIN) {

            if(count($dep_events) > 0) {

                $events_id = []; // related events id

                foreach ($dep_events as $event_id) {
                    $events_id[] = $event_id->id;
                }
                foreach ($events_id as $id) {

                    Event::where('id', $id)
                        ->delete();
                }
            }

            if(count($dep_users) > 0) {

                $users_id = []; // related users id

                foreach ($dep_users as $user_id) {
                    $users_id[] = $user_id->id;
                }

                foreach ($users_id as $id) {

                    User::where('id', $id)
                        ->delete();
                }
            }

            if ($dep_edit->delete()) {

                $message = 'Отдел удален вместе с зависимыми Событиями и пользователями';

                return $message;

            } else {

                $message = 'Отдел НЕ был Удален!!!';

                return $message;
            }
        }
    }
}
