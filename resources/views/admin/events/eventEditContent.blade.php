<hr class="my-4">
<br>
<h3 class="text-center">{{ $title }}</h3>
<h4></h4>
<br>
<hr class="my-4">

<div class="wrapper container-fluid">
    {!! Form::open(['url' => route('eventEdit', array('event'=>$data['id'])), 'class'=>'form-horizontal', 'method'=>'POST', 'enctype'=>'multipart/form-data' ])!!}

    {{--TITLE--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::hidden('id', $data['id']) !!}
            {!! Form::label('title', 'Title:', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::text('title', $data['title'], ['class' => 'form-control', 'placeholder'=>'Input title!']) !!}

            </div>
        </div>
    </div>
    <br>

    {{--DESCRIPTION--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('description', 'Description:', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('description', $data['description'], ['class' => 'form-control', 'placeholder'=>'Input description!']) !!}
            </div>
        </div>
    </div>
    <br>

    {{--DEPARTAMENT ID--}}
    @if($departaments)

        <div class="form_group">
            <div class="row">
                <div class="col-sm-1"></div>
                {!! Form::label('departament_id', 'Departament', ['class'=>'col-sm-1 control-label']) !!}
                <div class="col-sm-8">

{{--                    {!! Form::text('departament_id', $data['departament'], ['class' => 'form-control', 'placeholder'=>'Input Creator!']) !!}--}}
{{--                    {!! Form::text('departament_id', $old_departament_name, ['class' => 'form-control', 'placeholder'=>'Input Creator!']) !!}--}}

{{--                    <select class="form-control" id="departamentidSelect" name="departament_id" value="{{ $data['departament'] }}">--}}
                    <select class="form-control" id="departamentidSelect" name="departament_id" value="{{ $old_departament_name }}">

                        <option selected value="{{ $old_departament_id }}"> {{ $old_departament_name }} </option>

                        @foreach( $departaments as $departament)
                            <option value="{{ $departament->id }}"> {{ $departament->name }} </option>
                        @endforeach

                    </select>

                </div>
            </div>
        </div>

    @endif

    <br>

    {{--CREATOR--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('creator_id', 'Creator:', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
{{--                {!! Form::text('creator', $data['creator'], ['class' => 'form-control', 'placeholder'=>'Input Creator!']) !!}--}}
                {!! Form::text('creator', $old_creator_name, ['class' => 'form-control', 'placeholder'=>'']) !!}
            </div>
        </div>
    </div>
    <br>

    {{--REPORT--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('report', 'Report:', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('report', $data['report'], ['class' => 'form-control', 'placeholder'=>'Input report!']) !!}
            </div>
        </div>
    </div>
    <br>


    {{--PRIORITY--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('priority', 'Priority', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">

                {!! Form::select('priority', array(
                    'veryhigh' => 'VeryHigh',
                    'high' => 'High',
                    'middle' => 'Middle',
                    'low' => 'Low',
                    'usual' => 'Usual'
                    ), 'usual') !!}

            </div>
        </div>
    </div>

    {{--PEFORMER--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('user_id', 'Perfomer', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">

{{--                {!! Form::text('user_id', $old_event_perfomer_name, ['class' => 'form-control', 'placeholder'=>'Input Creator!']) !!}--}}

                <select class="form-control" id="perfomerSelect" name="user_id" value="{{ $old_event_perfomer_name }}">

                    <option selected value="{{ $old_departament_id }}"> {{ $old_event_perfomer_name }} </option>

                    @foreach( $performers as $performer)
                        <option value="{{ $performer->id }}"> {{ $performer->name }} </option>
                    @endforeach

                </select>

            </div>
        </div>
    </div>
    <br>


    {{--CALENDAR--}}

    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('remind_date', 'RemindDate', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--<div>--}}
                    {{--<input type="text" name="remind_date" class="tcal" value="{{ $data['remind_date'] }}" />--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}




    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('start', 'StartDate', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <div>
                    <input type="text" name="start" class="tcal" value="{{ $data['start'] }}" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('end', 'EndDate', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <div>
                    <input type="text" name="end" class="tcal" value="{{ $data['end'] }}" />
                </div>
            </div>
        </div>
    </div>
    {{--END CALENDAR--}}

    {{----}}
    {{--<div class="form_group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('executor', 'Executor:', ['class'=>'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--{!! Form::text('executor', $data['executor'], ['class' => 'form-control', 'placeholder'=>'Input executor!']) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}
{{----}}

    {{--SUBMIT BUTTON--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-2 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>
    <br>

    {!! Form::close() !!}

</div>



