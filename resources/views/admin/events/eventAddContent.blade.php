<h2 class="text-center">ADD NEW EVENT</h2>


@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<div class="wrapper container-fluid">

    {!! Form::open(['url'=>route('eventAdd'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

    {{--TITLE--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('title', 'Event Title', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('title', old('title'), ['class' => 'form-control',
                                        'placeholder'=>'Input title event']) !!}
            </div>
        </div>
    </div>

    {{--DESCRIPTION--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('description', 'Description', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('description', old('description'), ['class' => 'form-control',
                                        'placeholder'=>'Input Description']) !!}
            </div>
        </div>
    </div>


    {{--ID DEPARTAMENT--}}
    @if($departaments)

        <div class="form-group">
            <div class="row">
                <div class="col-sm-1"></div>
                {!! Form::label('departament_id', 'Departament', ['class' => 'col-sm-1 control-label']) !!}
                <div class="col-sm-8">
                    <div class="form-group">
                        <select class="form-control" id="departament_idSelect" name="departament_id">

                            @foreach( $departaments as $departament)
                                <option value="{{ $departament->id }}"> {{ $departament->name }} </option>
                            @endforeach

                        </select>
                    </div>

                </div>
            </div>
        </div>

    @endif

{{--SIMPLE CHOISE CATEGORY--}}
    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('event_category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--{!! Form::text('event_category_id', old('event_category_id'), ['class' => 'form-control',--}}
                                        {{--'placeholder'=>'Input category_id']) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--CATEGORY--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('event_category_id', 'Category', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <div class="form-group">
                    {{--<label for="exampleFormControlSelect1">Category select</label>--}}
                    <select class="form-control" id="categorySelect" name="category">

                        @foreach( $category as $cat)
                        <option value="{{ $cat->name }}"> {{ $cat->name }} </option>
                        @endforeach

                    </select>
                </div>
            </div>
        </div>
    </div>

    {{--Priority--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('priority', 'Priority', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <select class="form-control" id="prioritySelect" name="priority">
                    <option value="veryhigh">VeryHigh</option>
                    <option value="high">High</option>
                    <option value="middle">Middle</option>
                    <option value="low">Low</option>
                    <option value="usual">Usual</option>

                {{--{!! Form::select('priority', array(--}}
                    {{--'veryhigh' => 'VeryHigh',--}}
                    {{--'high' => 'High',--}}
                    {{--'middle' => 'Middle',--}}
                    {{--'low' => 'Low',--}}
                    {{--'usual' => 'Usual'--}}
                    {{--), 'usual') !!}--}}

                </select>

            </div>
        </div>
    </div>

    {{--PERFOMER ID--}}
    <div class="form_group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('user_id', 'Perfomer', ['class'=>'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <select class="form-control" id="perfomerSelect" name="user_id">

                    @foreach( $perfomers as $perfomer)
                        <option value="{{ $perfomer->id }}"> {{ $perfomer->name }} </option>
                    @endforeach

                </select>

            </div>
        </div>
    </div>
    <br>

    {{--REPORT--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('report', 'EventReport', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('report', old('report'), ['class' => 'form-control',
                                        'placeholder'=>'Input Report']) !!}
            </div>
        </div>
    </div>

{{--CALENDAR--}}

    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('remind_date', 'RemindDate', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <div>
                    <input type="text" name="remind_date" class="tcal" value="" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('start', 'StartDate', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <div>
                    <input type="text" name="start" class="tcal" value="" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('end', 'EndDate', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                <div>
                    <input type="text" name="end" class="tcal" value="" />
                </div>
            </div>
        </div>
    </div>
    {{--END CALENDAR--}}


    {{--<div class="form-group">--}}
        {{--<div class="row">--}}
            {{--<div class="col-sm-1"></div>--}}
            {{--{!! Form::label('author_id', 'Author', ['class' => 'col-sm-1 control-label']) !!}--}}
            {{--<div class="col-sm-8">--}}
                {{--{!! Form::text('author_id', old('author_id'), ['class' => 'form-control',--}}
                                        {{--'placeholder'=>'Введите author_id']) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}



    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-1"></div>--}}
    {{--{!! Form::label('images', 'Изображение', ['class' => 'col-sm-1 control-label']) !!}--}}
    {{--<div class="col-sm-6">--}}
    {{--{!! Form::file('images',['class'=>'filestyle','data-buttonText'=>'Выберите файл']) !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    {{--SAVE BUTTON--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-1 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

</div>


<div class="container-fluid up-cont dark-gr" style="height: 20px"></div>
