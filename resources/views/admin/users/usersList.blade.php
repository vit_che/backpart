<hr class="my-4">

<h2 class="text-center">USERS LIST</h2>


<div class="container-fluid">

    @if($users)

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th># Number</th>
                <th>name</th>
                <th>role</th>
                <th>departament</th>
                <th>email</th>
                <th>Data Created</th>
                <th>Delete</th>
            </tr>
            </thead>

            @foreach( $users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{!! Html::link(route('userEdit', ['user'=>$user->id]), $user->name,['alt'=>$user->name]) !!}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ $user->departament->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>
                        {!!  Form::open(['url' => route('userEdit', ['user'=>$user->id]), 'class'=>'form-horizontal', 'method'=>'POST']) !!}
                        {{ method_field('DELETE') }}
                        {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>

    @endif

    <button  type="button" class="bg-orange white btn btn-default bord "><span class="white">{!! Html::link(route('userAdd'), 'Create a New User') !!}</span></button>




</div>
